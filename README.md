# helloworld

Simple Hello World app written in nodejs

Terraform is used to provision an AWS t3.micro with a remote-exec script
to install a single node Kubernetes cluster (k3s)

local-exec is used to run Ansible via docker-compose
Ansible accesses the AWS service and retrieves Kubernetes auth and passes it to GitLab

Cloudflare is passed the t3.micro public IP and updates the A record for *.gitlab.arranshort.xyz

Requirements
-----
 - AWS CLI
 - Terraform
 - Docker
 - Docker-compose
 - SSH Pub Key in Gitlab
 - GitLab user Token in environment variables (GITLAB_TOKEN)
 - Cloudflare credentials  environment variables (CLOUDFLARE_EMAIL CLOUDFLARE_API_KEY)
 - AWS credentials in ~/.aws/configuration
 
Usage
-----
```
git clone https://gitlab.com/arranshort/helloworld`
cd helloworld
terraform apply
```



