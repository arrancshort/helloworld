FROM node:lts-alpine

WORKDIR /usr/src/app

COPY ./src .

EXPOSE 5000

CMD [ "node", "server.js" ]