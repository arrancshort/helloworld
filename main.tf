
data "http" "gitlab-pub-ssh-key" {
  url = "https://gitlab.com/arrancshort.keys"
}

resource "aws_key_pair" "gitlab_key" {
  key_name   = "gitlab"
  public_key = data.http.gitlab-pub-ssh-key.body
}

provider "aws" {
  profile = "default"
  region  = "eu-west-2"
}

resource "aws_security_group" "home" {
    name = "home"

    ingress { 
        from_port = 22    
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["84.92.32.217/32", "109.249.184.0/24"]
    }

    ingress { 
        from_port = 6443    
        to_port = 6443
        protocol = "tcp"
        cidr_blocks = ["84.92.32.217/32", "109.249.184.0/24"]
    }

}

resource "aws_security_group" "gitlab" {
    name = "gitlab"

    ingress { 
        from_port = 6443    
        to_port = 6443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        # cidr_blocks = [
        #   "8.34.208.0/20",
        #   "8.35.192.0/21",
        #   "8.35.200.0/23",
        #   "108.59.80.0/20",
        #   "108.170.192.0/20",
        #   "108.170.208.0/21",
        #   "162.216.148.0/22",
        #   "162.222.176.0/21",
        #   "173.255.112.0/20",
        #   "192.158.28.0/22",
        #   "199.192.112.0/22",
        #   "199.223.232.0/22",
        #   "199.223.236.0/23",
        #   "23.236.48.0/20",
        #   "23.251.128.0/19",
        #   "35.204.0.0/14",
        #   "35.208.0.0/13",
        #   "107.167.160.0/19",
        #   "107.178.192.0/18",
        #   "146.148.2.0/23",
        #   "146.148.4.0/22",
        #   "146.148.8.0/21",
        #   "146.148.16.0/20",
        #   "146.148.32.0/19",
        #   "146.148.64.0/18",
        #   "35.203.0.0/17",
        #   "35.203.128.0/18",
        #   "35.203.192.0/19",
        #   "35.203.240.0/20",
        #   "130.211.8.0/21",
        #   "130.211.16.0/20",
        #   "130.211.32.0/19",
        #   "130.211.64.0/18",
        #   "130.211.128.0/17",
        #   "104.154.0.0/15",
        #   "104.196.0.0/14",
        #   "208.68.108.0/23",
        #   "35.184.0.0/14",
        #   "35.188.0.0/15",
        #   "35.216.128.0/17",
        #   "35.217.128.0/17",
        #   "35.190.0.0/17",
        #   "35.190.128.0/18",
        #   "35.190.192.0/19",
        #   "35.235.224.0/20",
        #   "35.192.0.0/14",
        #   "35.196.0.0/15",
        #   "35.198.0.0/16",
        #   "35.199.0.0/17",
        #   "35.199.128.0/18",
        #   "35.200.0.0/15",
        #   "35.235.216.0/21"
        # ]
    }
}

resource "aws_security_group" "ingress_globally_accessible" {
    name = "ingress_globally_accessible"

    ingress { 
        from_port = 80   
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress { 
        from_port = 443   
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_security_group" "egress_globally_accessible" {
    name = "egress_globally_accessible"

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_instance" "helloworld" {
  ami           = "ami-0ed4a9453b39ea8c1"
  instance_type = "t3.micro"
  key_name      = "gitlab"
  vpc_security_group_ids = [
      aws_security_group.home.id,
      aws_security_group.gitlab.id,
      aws_security_group.ingress_globally_accessible.id,
      aws_security_group.egress_globally_accessible.id
      ]
  connection {
    type        = "ssh"
    user        = "ubuntu"
    host        = aws_instance.helloworld.public_ip
    private_key = file("~/.ssh/id_rsa")
  }

  provisioner "remote-exec" {
    inline = [
        "curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.17.13+k3s2 sh -"
    ]
  }

  provisioner "local-exec" {
    command = "GITLAB_TOKEN=`echo $GITLAB_TOKEN` VARIABLE_HOST=${aws_instance.helloworld.public_ip} docker-compose -f ./ansible/docker-compose.yml up --build"
  }

}
