provider "cloudflare" {
    account_id = "577de65c275cf820abc4eb06578145f1"
}

resource "cloudflare_record" "gitlab_wildcard" {
  zone_id = "be3a49a2b006d5d1a4691c61c710363f"
  name    = "*.gitlab"
  value   = aws_instance.helloworld.public_ip
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "hello" {
  zone_id = "be3a49a2b006d5d1a4691c61c710363f"
  name    = "hello"
  value   = aws_instance.helloworld.public_ip
  type    = "A"
  proxied = false
}